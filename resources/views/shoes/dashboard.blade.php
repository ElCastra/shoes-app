@extends('components.layouts.app')

@section('title', 'Dashboard')

{{-- style --}}
@section('styles')
    @vite('resources/css/app.css')
    @vite('resources/css/components.css')
    @vite('resources/css/aos.css')
    @vite('resources/css/normalize.css')
@endsection

@section('navbar')
    <nav class="bg-red-500 text-white flex justify-center">
        <h1 class="font-figtree text-3xl">Dashboard</h1>
    </nav>
@endsection

@section('main')
    <h2 class="text-center font-bold">Testing</h2>
@endsection
