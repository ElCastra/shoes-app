<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('images/shoe-icon.svg') }}">

    <title>@yield('title', 'Shoe Store')</title>
    <title>{{ $title ?? 'Page Title' }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    <!-- Styles -->
    @yield('styles')
    @if (isset($slot))
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.css">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @endif

</head>

<body class="scroll-smooth bg-slate-200">
    @yield('navbar')
    @yield('main')
    @yield('scripts')

    @if (isset($slot))
        {{ $slot }}
    @endif
</body>

</html>
