@extends('components.layouts.app')

@section('title', 'Selamat Datang di Shoes Store!')

{{-- Style --}}
@section('styles')
    @vite('resources/css/app.css')
    @vite('resources/css/components.css')
    @vite('resources/css/aos.css')
    @vite('resources/css/normalize.css')
@endsection


{{-- Navbar --}}
@section('navbar')

    <nav class="bg-red-500 text-white flex flex-row justify-between items-center w-full">
        <h1 class="font-figtree lg:text-3xl text-xl relative lg:left-16 left-5">Shoes-App</h1>
        <a class="text-2xl md:text-4xl relative sm:right-20 glow" href="{{ route('shoes.dashboard') }}"><span
                class=" icon-[material-symbols-light--remove-shopping-cart-rounded]"></span></a>
    </nav>
@endsection

{{-- Content --}}
@section('main')
    <div class=" h-10 md:h-20"></div>
    <div class="flex sm:flex-row flex-col transition-all justify-between">
        <div id="img" class="flex items-center justify-center sm:items-start relative lg:left-28" data-aos="fade-right">
            <img src="{{ asset('images/blue-shoes.webp') }}" class="w-[35vw] h-auto" />
        </div>
        <div id="text" class="flex flex-col items-center md:items-center text-center relative md:right-20 lg:right-28"
            data-aos="fade-left">
            <h1 class="md:text-5xl 2xl:text-8xl font-figtree font-bold ">Selamat Datang!</h1>
            <p class="md:text-2xl 2xl:text-4xl font-figtree">Temukan sepatu yang cocok untukmu</p>

            <button
                class="md:w-28 md:h-10 rounded-lg p-2 shadow-lg mt-8 active:shadow-none hover:bg-slate-400 active:scale-95"
                onclick="window.location.href='/dashboard'">Cari Sepatu!</button>
        </div>
    </div>


    <div id="space" class="h-[150px] w-full"></div>
    <div id="card" class="flex flex-col justify-center text-center">
        <div class="relative">
            <img class="w-full h-[380px] object-cover z-0 opacity-85 rounded-lg" src="{{ asset('images/D-19.webp') }}" />
            <div class="absolute inset-0 flex flex-col justify-center items-center z-10" data-aos="fade-down">
                <h2 class="md:text-5xl font-figtree font-bold text-white mb-5 text-stroke">Why Choose Us?</h2>
                <div class="flex justify-center space-x-8">
                    <div class="bg-sky-200 p-4 rounded-lg shadow-md h-[200px]">
                        <h3 class="font-bold mb-2">Fast</h3>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                    <div class="bg-sky-200 p-4 rounded-lg shadow-md">
                        <h3 class="font-bold mb-2">Efficient</h3>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                    <div class="bg-sky-200 p-4 rounded-lg shadow-md">
                        <h3 class="font-bold mb-2">Secure</h3>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="space" class="h-[250px] w-full"></div>

        <div class="flex space-x-16 animate-loop-scroll">
            <div class="flex space-x-16 animate-loop-scroll">
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/8ee9f161-df19-4fa7-a2a6-edf9acf0e0d6?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 1" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/80480f8a-69ad-4c30-88ba-f4e7ee08fc51?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 2" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/140d376c-13f2-4823-b397-b3de733bf560?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 3" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/0ae217f1-b695-4661-bd3d-6440eebc2c5c?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 4" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/67017079-51e1-4245-9bf1-b5957eb66c74?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 5" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/515313ac-7ec9-4c6e-95db-80dac2f8b960?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 6" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/c513fc32-3ab9-4cca-911e-0b2642ac7206?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 7" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/5731a5a7-689f-49ae-abf1-6e6dc00c2043?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 8" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/cb51d286-530f-42be-9e91-9c850522f127?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 9" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/44ba8437-f6fd-4a51-bfd3-262d7528f7a4?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 10" />
            </div>
            <div class="flex space-x-16 animate-loop-scroll" aria-hidden="true">
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/8ee9f161-df19-4fa7-a2a6-edf9acf0e0d6?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 1" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/80480f8a-69ad-4c30-88ba-f4e7ee08fc51?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 2" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/140d376c-13f2-4823-b397-b3de733bf560?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 3" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/0ae217f1-b695-4661-bd3d-6440eebc2c5c?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 4" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/67017079-51e1-4245-9bf1-b5957eb66c74?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 5" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/515313ac-7ec9-4c6e-95db-80dac2f8b960?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 6" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/c513fc32-3ab9-4cca-911e-0b2642ac7206?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 7" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/5731a5a7-689f-49ae-abf1-6e6dc00c2043?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 8" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/cb51d286-530f-42be-9e91-9c850522f127?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 9" />
                <img loading="lazy"
                    src="https://cdn.builder.io/api/v1/image/assets/TEMP/44ba8437-f6fd-4a51-bfd3-262d7528f7a4?apiKey=7e8b177c7c374d8abaf3aebf27f1c17d&"
                    class="max-w-none" alt="Image 10" />
            </div>
        </div>


    @endsection

    {{-- JS Script --}}

    @section('scripts')
        @vite('resources/js/app.js')
    @endsection
