<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShoeController;

Route::get('/', function () {
    return view('index');
});

Route::get('/dashboard', [ShoeController::class, 'dashboard'])->name('shoes.dashboard');
